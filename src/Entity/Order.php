<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $recipient_name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $recipient_zip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $recipient_city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $recipient_street;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $recipient_streetNumber;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creation_datetime;

    /**
     * @ORM\OneToMany(targetEntity=DHLLabel::class, mappedBy="ProductOrder")
     */
    private $dHLLabels;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $vinted_item_id;

    /**
     * @ORM\OneToOne(targetEntity=Payment::class, inversedBy="productOrder", cascade={"persist", "remove"})
     */
    private $payment;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $porto_price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dhlpakid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customerToken;

    public function __construct()
    {
        $this->dHLLabels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRecipientName(): ?string
    {
        return $this->recipient_name;
    }

    public function setRecipientName(?string $recipient_name): self
    {
        $this->recipient_name = $recipient_name;

        return $this;
    }

    public function getRecipientZip(): ?int
    {
        return $this->recipient_zip;
    }

    public function setRecipientZip(?int $recipient_zip): self
    {
        $this->recipient_zip = $recipient_zip;

        return $this;
    }

    public function getRecipientCity(): ?string
    {
        return $this->recipient_city;
    }

    public function setRecipientCity(?string $recipient_city): self
    {
        $this->recipient_city = $recipient_city;

        return $this;
    }

    public function getRecipientStreet(): ?string
    {
        return $this->recipient_street;
    }

    public function setRecipientStreet(string $recipient_street): self
    {
        $this->recipient_street = $recipient_street;

        return $this;
    }

    public function getRecipientStreetNumber(): ?int
    {
        return $this->recipient_streetNumber;
    }

    public function setRecipientStreetNumber(?int $recipient_streetNumber): self
    {
        $this->recipient_streetNumber = $recipient_streetNumber;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getCreationDatetime(): ?\DateTimeInterface
    {
        return $this->creation_datetime;
    }

    public function setCreationDatetime(\DateTimeInterface $creation_datetime): self
    {
        $this->creation_datetime = $creation_datetime;

        return $this;
    }

    /**
     * @return Collection|DHLLabel[]
     */
    public function getDHLLabels(): Collection
    {
        return $this->dHLLabels;
    }

    public function addDHLLabel(DHLLabel $dHLLabel): self
    {
        if (!$this->dHLLabels->contains($dHLLabel)) {
            $this->dHLLabels[] = $dHLLabel;
            $dHLLabel->setProductOrder($this);
        }

        return $this;
    }

    public function removeDHLLabel(DHLLabel $dHLLabel): self
    {
        if ($this->dHLLabels->removeElement($dHLLabel)) {
            // set the owning side to null (unless already changed)
            if ($dHLLabel->getProductOrder() === $this) {
                $dHLLabel->setProductOrder(null);
            }
        }

        return $this;
    }

    public function getVintedItemId(): ?string
    {
        return $this->vinted_item_id;
    }

    public function setVintedItemId(string $vinted_item_id): self
    {
        $this->vinted_item_id = $vinted_item_id;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPortoPrice(): ?string
    {
        return $this->porto_price;
    }

    public function setPortoPrice(?string $porto_price): self
    {
        $this->porto_price = $porto_price;

        return $this;
    }

    public function getDhlpakid(): ?string
    {
        return $this->dhlpakid;
    }

    public function setDhlpakid(?string $dhlpakid): self
    {
        $this->dhlpakid = $dhlpakid;

        return $this;
    }

    public function getCustomerToken(): ?string
    {
        return $this->customerToken;
    }

    public function setCustomerToken(string $customerToken): self
    {
        $this->customerToken = $customerToken;

        return $this;
    }
}
