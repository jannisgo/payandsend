<?php

namespace App\Entity;

use App\Repository\DHLLabelRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DHLLabelRepository::class)
 */
class DHLLabel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dhlLabelID;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="dHLLabels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ProductOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDhlLabelID(): ?string
    {
        return $this->dhlLabelID;
    }

    public function setDhlLabelID(string $dhlLabelID): self
    {
        $this->dhlLabelID = $dhlLabelID;

        return $this;
    }

    public function getProductOrder(): ?Order
    {
        return $this->ProductOrder;
    }

    public function setProductOrder(?Order $ProductOrder): self
    {
        $this->ProductOrder = $ProductOrder;

        return $this;
    }
}
