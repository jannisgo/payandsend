<?php


namespace App\Entity;


class DHLItemType
{
    private string $id;
    private int $maxDepth;
    private int $maxLength;
    private int $maxWidth;
    private int $maxWeight;
    private bool $tracking;
    private string $displayName;
    private float $amount;

    /**
     * DHLItemType constructor.
     * @param string $id
     * @param int $maxDepth
     * @param int $maxLength
     * @param int $maxHeight
     * @param int $maxWidth
     * @param bool $tracking
     * @param string $displayName
     * @param float $amount
     */
    public function __construct(string $id, int $maxDepth, int $maxLength, int $maxWidth, int $maxWeight, bool $tracking, string $displayName, float $amount)
    {
        $this->id = $id;
        $this->maxDepth = $maxDepth;
        $this->maxLength = $maxLength;
        $this->maxWidth = $maxWidth;
        $this->maxWeight = $maxWeight;
        $this->tracking = $tracking;
        $this->displayName = $displayName;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMaxDepth(): int
    {
        return $this->maxDepth;
    }

    /**
     * @return int
     */
    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    /**
     * @return int
     */
    public function getMaxWidth(): int
    {
        return $this->maxWidth;
    }

    /**
     * @return bool
     */
    public function isTracking(): bool
    {
        return $this->tracking;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getMaxWeight(): int
    {
        return $this->maxWeight;
    }



}