<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="payments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fulfillment_date;

    /**
     * @ORM\OneToOne(targetEntity=Order::class, mappedBy="payment", cascade={"persist", "remove"})
     */
    private $productOrder;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFulfillmentDate(): ?\DateTimeInterface
    {
        return $this->fulfillment_date;
    }

    public function setFulfillmentDate(?\DateTimeInterface $fulfillment_date): self
    {
        $this->fulfillment_date = $fulfillment_date;

        return $this;
    }

    public function getProductOrder(): ?Order
    {
        return $this->productOrder;
    }

    public function setProductOrder(?Order $productOrder): self
    {
        // unset the owning side of the relation if necessary
        if ($productOrder === null && $this->productOrder !== null) {
            $this->productOrder->setPayment(null);
        }

        // set the owning side of the relation if necessary
        if ($productOrder !== null && $productOrder->getPayment() !== $this) {
            $productOrder->setPayment($this);
        }

        $this->productOrder = $productOrder;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
