<?php

namespace App\Repository;

use App\Entity\DHLLabel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DHLLabel|null find($id, $lockMode = null, $lockVersion = null)
 * @method DHLLabel|null findOneBy(array $criteria, array $orderBy = null)
 * @method DHLLabel[]    findAll()
 * @method DHLLabel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DHLLabelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DHLLabel::class);
    }

    // /**
    //  * @return DHLLabel[] Returns an array of DHLLabel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DHLLabel
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
