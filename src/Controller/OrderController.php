<?php


namespace App\Controller;


use App\Entity\DHLItemType;
use App\Entity\Order;
use App\Entity\Payment;
use App\Entity\User;
use DOMDocument;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class OrderController
 * @package App\Controller
 * @Route("/orders")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/")
     * @IsGranted("ROLE_USER")
     */
    function index(){
        $orders = $this->getDoctrine()->getRepository(Order::class)->findBy([
            "creator" => $this->getUser()
        ]);

        return $this->render('content/orders/list.html.twig', [
            "orders" => $orders,
            "title" => "Produkte / Bestellungen"
        ]);
    }

    /**
     * @Route("/{id}")
     * @IsGranted("ROLE_USER")
     */
    function order($id){
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        if($order == null){
            $this->addFlash('error', "Die gesuchte Bestellung konnte nicht gefunden werden!");
            return $this->redirectToRoute('app_order_index');
        }
        return $this->render('content/orders/details.html.twig', [
            "title" => $order->getName(),
            "order" => $order
        ]);
    }

    /**
     * @Route("/{id}/payment")
     */
    function payment_manual($id, Request $request){
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        if($order == null){
            $this->addFlash('error', "Die gesuchte Bestellung konnte nicht gefunden werden!");
            return $this->redirectToRoute('app_order_index');
        }

        $payment = new Payment();
        $payment->setFulfillmentDate(new \DateTime());
        $payment->setAmount($order->getPrice());

        $form = $this->createForm('App\Form\PaymentFormType', $payment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $payment->setUser($this->getUser());
            $payment->setProductOrder($order);
            $payment->setFulfillmentDate(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            return $this->redirectToRoute('app_order_order', ["id" => $order->getId()]);
        }else{
            return $this->render('content/orders/manuallyPaid.html.twig', [
                "title" => $order->getName(),
                "order" => $order,
                "form" => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/{id}/payPalPaid/{state}")
     */
    function setPayPalPaid($id, $state)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);

        if($state == "COMPLETED") {
            $payment = new Payment();
            $payment->setType('PayPal');
            $payment->setAmount($order->getPrice());
            if ($order->getPortoPrice() != null) {
                $payment->setAmount($payment->getAmount() + $order->getPortoPrice());
            }
            $payment->setProductOrder($order);
            $payment->setUser($order->getCreator());
            $payment->setFulfillmentDate(new \DateTime());
            $em->persist($payment);
            $em->flush();
            $order->setPayment($payment);
            $em->persist($order);
            $em->flush();
        }
        $this->addFlash('checkmark', '');
        return $this->json(["result" => "success"]);
    }

    /**
     * @Route("/{id}/setPost")
     * @IsGranted("ROLE_USER")
     */
    function setPost($id, HttpClientInterface $client, Request $request){
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        if($order == null){
            $this->addFlash('error', "Die gesuchte Bestellung konnte nicht gefunden werden!");
            return $this->redirectToRoute('app_order_index');
        }

        if($request->get('submitted')){
            $order->setPortoPrice($request->get('postCost'));
            if($request->get('pack_type') == "custom"){
                $order->setDhlpakid(null);
            }else{
                $order->setDhlpakid($request->get('pack_type'));
            }

            if($order->getPortoPrice() == 0){
                $order->setPortoPrice(null);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            if($request->getSession()->has('redirectAfterAction')){
                $url = $request->getSession()->get('redirectAfterAction');
                $request->getSession()->remove('redirectAfterAction');
                return $this->redirect($url);
            }else{
                return $this->redirectToRoute('app_order_order', ['id' => $id]);
            }
        }

        $host = $this->getParameter('dhl.host');
        $user = $this->getParameter('dhl.user');
        $password = $this->getParameter('dhl.password');
        $partnerid = $this->getParameter('dhl.partnerid');
        $panelUser = $this->getParameter('dhl.panel_user');
        $panelPass = $this->getParameter('dhl.panel_pass');


        $response = $client->request('GET', $host . '/privatecustomershipping/partners/' . $partnerid . '/catalog/current/products', [
            'auth_basic' => [$panelUser, $panelPass],
            'headers' => [
                'DPDHL-User-Authentication-Token' => 'Basic ' . base64_encode($user . ':' . $password),
            ]
        ]);
        $products = $response->toArray()["products"];
        /** @var DHLItemType[] $productEntities */
        $productEntities = [];

        foreach ($products as $id => $product){
            $attr = $product['attributes'];

            $price = 0.0;
            $de_avail = false;

            $regions = $product['regions'];
            foreach ($regions as $region){
                if(in_array('DEU', $region['countries'])){
                    $price = $region['price']['amount'];
                    $de_avail = true;
                }
            }

            if($de_avail){
                array_push($productEntities, new DHLItemType($id, $attr["maxDepth"], $attr["maxLength"], $attr["maxWidth"], $attr["maxWeight"], $attr["tracking"], $attr["displayName"]["text"], $price));
            }
        }

        return $this->render('content/orders/setPostage.html.twig', [
            "title" => $order->getName(),
            "order" => $order,
            "products" => $productEntities
        ]);
    }

    /**
     * @Route("/{id}/delete")
     */
    function delete($id){
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($order);
        $em->flush();

        $this->addFlash('success', 'Der Eintrag wurde erfolgreich gelöscht!');

        return $this->redirectToRoute('app_order_index');
    }

    /**
     * @Route("/import/vinted")
     * https://www.vinted.de/damen/kleidung/blazer-and-anzuge-532/1250759212-blazer-zara-beige
     */
    function import_vinted(Request $request, HttpClientInterface $client){
        $user = $this->getUser();
        if($user == null){
            if($request->headers->get('X-API-Token')){
                $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["APIToken" => $request->headers->get('X-API-Token')]);
                if($user == null){
                    return $this->json(["success" => false, "error" => "no user found"]);
                }
            }else{
                $this->denyAccessUnlessGranted("ROLE_USER");
            }
        }
        $vintedURL = $request->query->get('url');

        $urlParts = explode('/', $vintedURL);
        $vintedID = explode('-', $urlParts[sizeof($urlParts)-1])[0];

        $html = file($vintedURL);

        $amount = 0.0;
        $title = "";

        foreach ($html as $line){
            if(str_contains($line, "og:price:amount")){
                $value = explode("\"", $line)[3];
                $amount = $value;
            }

            if(str_contains($line, "og:title")){
                $value = explode("\"", $line)[3];
                $title = $value;
            }
        }

        $checkExisting = $this->getDoctrine()->getRepository(Order::class)->findOneBy(["vinted_item_id" => $vintedID]);
        if($checkExisting == null){
            $order = new Order();
            $order->setVintedItemId($vintedID);
            $order->setName($title);
            $order->setCreator($user);
            $order->setPrice($amount);
            $order->setCreationDatetime(new \DateTime());
            $order->setCustomerToken(md5(random_bytes(20)));

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            if($request->query->has('headless')){
                return $this->json($order);
            }

            $this->addFlash('checkmark', '');
            $request->getSession()->set('redirectAfterAction', $this->generateUrl('app_order_order', ['id' => $order->getId()]));
            return $this->redirectToRoute('app_order_setpost', ['id' => $order->getId()]);
        }else{
            if($request->query->has('headless')){
                return $this->json(["customerToken" => $checkExisting->getCustomerToken()]);
            }
            $this->addFlash('error', 'Dieser Artikel existiert schon. Du wurdest zu diesem weitergeleitet!');
            return $this->redirectToRoute('app_order_setpost', ['id' => $checkExisting->getId()]);
        }
    }
}