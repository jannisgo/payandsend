<?php

namespace App\Controller;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('content/security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'title' => "Login"]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/me")
     * @IsGranted("ROLE_USER")
     */
    function me(Request $request){
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
        $form = $this->createForm('App\Form\UserFormType', $user);
        $form->handleRequest($request);

        $addressForm = $this->createForm('App\Form\UserAddressType', $user);
        $addressForm->handleRequest($request);

        if($addressForm->isSubmitted() && $addressForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Dein Profil wurde erfolgreich aktualisiert!');
        }

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Dein Profil wurde erfolgreich aktualisiert!');
        }

        return $this->render('content/me.html.twig', [
            "form" => $form->createView(),
            "title" => "Profil bearbeiten",
            "addressForm" => $addressForm->createView()
        ]);
    }

    /**
     * @Route("/me/generateToken")
     * @IsGranted("ROLE_USER")
     */
    function generateToken(){
        $user = $this->getUser();
        $user->setAPIToken(md5(random_bytes(50)));
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('app_security_me');
    }
}
