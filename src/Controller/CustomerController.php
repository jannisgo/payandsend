<?php


namespace App\Controller;


use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CustomerController extends AbstractController
{
    /**
     * @Route("/customer/{token}")
     */
    function customer_addressForm($token, \Symfony\Component\HttpFoundation\Request $request){
        $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy(["customerToken" => $token]);
        if($order == null){
            $this->addFlash('error', 'Dieser Link existiert nicht mehr!');
            return $this->redirectToRoute('app_staticpage_index');
        }

        $view = null;

        if($order->getPayment() != null){
            $form = $this->createForm('App\Form\CustomerDetailFormType', $order);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($order);
                $em->flush();

                $this->addFlash('checkmark', '');
            }

            $view = $form->createView();
        }

        return $this->render('content/customer/start.html.twig', [
            "title" => $order->getName(),
            "order" => $order,
            "form" => $view
        ]);
    }
}